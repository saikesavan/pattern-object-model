package org.testleaf.leaftaps.base;

import org.testleaf.leaftaps.componentbase.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testleaf.leaftaps.utils.ReadExcel;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class ProjectBase extends Component{

	public String excelFileName;
	public static String fName1;
	public String excelSheetName;

//	public static ChromeDriver driver;

	@BeforeMethod
	public void launchApp() {

		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Maven\\MavenProject\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@AfterMethod
	public void closeApp() {
		driver.quit();
	}

	@DataProvider(name = "fetchData")
	public String[][] sendData() throws IOException {
		ReadExcel excel = new ReadExcel();
		return excel.excelRead(excelFileName, excelSheetName);
	}

}
