package org.testleaf.leaftaps.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testleaf.leaftaps.base.ProjectBase;

public class CreateLead extends ProjectBase {
	@FindBy(id = "createLeadForm_companyName")
	WebElement companyName;
	
	@FindBy(id = "createLeadForm_firstName")
	WebElement firstName;
	
	@FindBy(id = "createLeadForm_lastName")
	WebElement lastName;	

	@FindBy(xpath = "(//input[@id='createLeadForm_generalPostalCodeExt'])/following::input")
	WebElement createLeadButton;	
	
	
	public CreateLead(WebDriver driver) 
	{
    PageFactory.initElements(driver, this);
	}
	
	
	
	public CreateLead enterTheCompanyName(String cName) {
	//	companyName.sendKeys(cName);
		clearAndType(companyName, cName);
	
	//	driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		return this;
	}

	public  CreateLead enterTheFirstName(String fName) {
	//	driver.findElementById("createLeadForm_firstName").sendKeys(fName);
    	//firstName.sendKeys(fName);
		clearAndType(firstName, fName);
		fName1 = firstName.getText();
	//	fName1 = driver.findElementById("createLeadForm_firstName").getText();
		return this;
	}

	public CreateLead enterTheLastName(String lName) {
		//driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	//	lastName.sendKeys(lName);
		clearAndType(lastName, lName);
		return this;
	}

	public ViewLead clickOnCreateLeadButton() {
	//	driver.findElementByXPath("(//input[@id='createLeadForm_generalPostalCodeExt'])/following::input").click();
      click(createLeadButton);
		return new ViewLead();

	}

}
