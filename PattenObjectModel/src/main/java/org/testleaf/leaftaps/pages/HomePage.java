package org.testleaf.leaftaps.pages;

//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.support.PageFactory;

import org.testleaf.leaftaps.base.ProjectBase;

public class HomePage extends ProjectBase{

	
	public HomePage verifyLoginName() {
		
		if(driver.findElementByTagName("h2").getText().contains("Demo Sales Manager")) {
			System.out.println("Login Verified");
		}
		else
		{
			System.out.println("Login Mismatch");
		}
		return this;
		
	}
	
	public MyHome clickCRMSFA()
    {
    
	driver.findElementByLinkText("CRM/SFA").click();
    	return new MyHome();
		
	}
	
	
	public LoginPage logOut() {
		driver.findElementByClassName("decorativeSubmit").click();
		return new LoginPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
