package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class MyLeads extends ProjectBase{

	public MyLeads() {

	}
	
	public CreateLead verifyLoginName() {
		
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLead(driver);
		
	}
	

}
