package org.testleaf.leaftaps.testcase;

import org.testleaf.leaftaps.base.ProjectBase;
import org.testleaf.leaftaps.pages.LoginPage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC001_LoginLogout extends ProjectBase {
	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "createTestLead";
	}
	
	@Test(dataProvider = "fetchData")
	public void runLogin(String cName, String fName, String lName) {
		/*LoginPage page = new LoginPage();
		page.enterUserName();
		page.enterPassword();
		page.clickLoginButton();*/
		
		
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLoginButton()
		.verifyLoginName()
		.clickCRMSFA()
		.clickLeads()
		.verifyLoginName()
		.enterTheCompanyName(cName)
		.enterTheFirstName(fName)
		.enterTheLastName(lName)
		.clickOnCreateLeadButton()
		.verifyFirstName();
		
	}

}
